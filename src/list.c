#include <stdlib.h>
#include <stdio.h>
#include "list.h"

list_t list_empty(){
  list_t res=NULL;
  return res;
}

int list_is_empty(list_t l){
  if(l==NULL){
    return 1;
  }
  return 0;
}

list_t list_push(list_t l, void* x){
  list_t res=malloc(sizeof(struct cell_t));

  if(list_is_empty(l)==1){
    res->val=x;
    res->next=NULL;
    res->id=1;
    return res;
  }

  res->val=x;
  res->next=l;
  res->id=l->id+1;
  return res;
}

list_t list_tail(list_t l){
  if(list_is_empty(l)==0){
    return l->next;
  }
  return NULL;
}

void* list_pop(list_t* l){
  if(list_is_empty(l)==0){
    list_t tmp = (*l);
    (*l)=(*l)->next;
    void* res= tmp->val;
    free(tmp);
    return res;
  }
  return NULL;
}

void* list_top(list_t l){
  if(list_is_empty(l)==0){
    return l->val;
  }
  return NULL;
}

void list_destroy(list_t l, void (*free_void)(void*)){
  while(list_is_empty(l)==0){
    free_void(l->val);
    list_t tmp=l;
    l=l->next;
    free(tmp);
  }
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  if(list_is_empty(l)==0){
    while(l->id!=1){
      if(eq(l->val,x)==0){
        return l->val;
      }
      l=l->next;
    }
  }
  return NULL;
}

unsigned long int list_len(list_t l){
  if(list_is_empty(l)==0){
    return l->id;
  }

  return 0;
}
